package com.xjj.framework.sys.dict.dao;

import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.sys.dict.entity.DictItem;


public interface DictDao extends XjjDAO<DictItem> {


}
