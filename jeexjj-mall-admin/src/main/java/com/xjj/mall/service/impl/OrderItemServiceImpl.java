/****************************************************
 * Description: ServiceImpl for t_mall_order_item
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.xjj.framework.dao.XjjDAO;
import com.xjj.framework.service.XjjServiceSupport;
import com.xjj.mall.entity.OrderItemEntity;
import com.xjj.mall.dao.OrderItemDao;
import com.xjj.mall.service.OrderItemService;

@Service
public class OrderItemServiceImpl extends XjjServiceSupport<OrderItemEntity> implements OrderItemService {

	@Autowired
	private OrderItemDao orderItemDao;

	@Override
	public XjjDAO<OrderItemEntity> getDao() {
		
		return orderItemDao;
	}
}