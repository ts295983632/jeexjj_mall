/****************************************************
 * Description: Entity for t_mall_log
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/

package com.xjj.mall.entity;

import java.util.Date;
import com.xjj.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class LogEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;
    public LogEntity(){}
    private String name;//name
    private Integer type;//type
    private String url;//url
    private String requestType;//request_type
    private String requestParam;//request_param
    private String user;//user
    private String ip;//ip
    private String ipInfo;//ip_info
    private Integer time;//time
    private Date createDate;//create_date
    /**
     * 返回name
     * @return name
     */
    public String getName() {
        return name;
    }
    
    /**
     * 设置name
     * @param name name
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * 返回type
     * @return type
     */
    public Integer getType() {
        return type;
    }
    
    /**
     * 设置type
     * @param type type
     */
    public void setType(Integer type) {
        this.type = type;
    }
    
    /**
     * 返回url
     * @return url
     */
    public String getUrl() {
        return url;
    }
    
    /**
     * 设置url
     * @param url url
     */
    public void setUrl(String url) {
        this.url = url;
    }
    
    /**
     * 返回request_type
     * @return request_type
     */
    public String getRequestType() {
        return requestType;
    }
    
    /**
     * 设置request_type
     * @param requestType request_type
     */
    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
    
    /**
     * 返回request_param
     * @return request_param
     */
    public String getRequestParam() {
        return requestParam;
    }
    
    /**
     * 设置request_param
     * @param requestParam request_param
     */
    public void setRequestParam(String requestParam) {
        this.requestParam = requestParam;
    }
    
    /**
     * 返回user
     * @return user
     */
    public String getUser() {
        return user;
    }
    
    /**
     * 设置user
     * @param user user
     */
    public void setUser(String user) {
        this.user = user;
    }
    
    /**
     * 返回ip
     * @return ip
     */
    public String getIp() {
        return ip;
    }
    
    /**
     * 设置ip
     * @param ip ip
     */
    public void setIp(String ip) {
        this.ip = ip;
    }
    
    /**
     * 返回ip_info
     * @return ip_info
     */
    public String getIpInfo() {
        return ipInfo;
    }
    
    /**
     * 设置ip_info
     * @param ipInfo ip_info
     */
    public void setIpInfo(String ipInfo) {
        this.ipInfo = ipInfo;
    }
    
    /**
     * 返回time
     * @return time
     */
    public Integer getTime() {
        return time;
    }
    
    /**
     * 设置time
     * @param time time
     */
    public void setTime(Integer time) {
        this.time = time;
    }
    
    /**
     * 返回create_date
     * @return create_date
     */
    public Date getCreateDate() {
        return createDate;
    }
    
    /**
     * 设置create_date
     * @param createDate create_date
     */
    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
    

    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("com.xjj.mall.entity.LogEntity").append("ID="+this.getId()).toString();
    }
}

