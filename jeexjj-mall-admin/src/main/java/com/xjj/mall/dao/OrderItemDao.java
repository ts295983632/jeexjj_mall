/****************************************************
 * Description: DAO for t_mall_order_item
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
package com.xjj.mall.dao;

import com.xjj.mall.entity.OrderItemEntity;

import java.util.List;

import com.xjj.framework.dao.XjjDAO;

public interface OrderItemDao  extends XjjDAO<OrderItemEntity> {
	
}

