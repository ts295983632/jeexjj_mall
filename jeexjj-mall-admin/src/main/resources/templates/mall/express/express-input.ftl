<#--
/****************************************************
 * Description: 商品描述表的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl"> 

<@input url="${base}/mall/express/save" id=tabId>
   <input type="hidden" name="id" value="${express.id}"/>
   
   <@formgroup title='商品描述'>
	<input type="text" name="expressName" value="${express.expressName}" >
   </@formgroup>
   <@formgroup title='排序'>
	<input type="text" name="sortOrder" value="${express.sortOrder}" check-type="number">
   </@formgroup>
   <@formgroup title='创建时间'>
	<@date name="created" dateValue=express.created  default=true/>
   </@formgroup>
   <@formgroup title='更新时间'>
	<@date name="updated" dateValue=express.updated  default=true/>
   </@formgroup>
</@input>